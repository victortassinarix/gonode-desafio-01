const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();

nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));
app.use(bodyParser.urlencoded({ urlencoded: false }));


const middlware = (req, res, next) => {
  if (!req.query.nome || !req.query.nome.trim().length) {
    res.redirect('/');
  } else {
    next();
  }
};

app.get('/', (req, res) => {
  res.render('main');
});


app.post('/check', (req, res) => {
  if (!req.body.dataNascimento || !req.body.nome) {
    res.redirect('/');
  }

  const data = moment(req.body.dataNascimento, 'DD/MM/YYYY');
  const idade = moment().diff(data, 'years');

  if (idade >= 18) {
    res.redirect(`/major?nome=${req.body.nome}`)
  } else {
    res.redirect(`/minor?nome=${req.body.nome}`)
  }
});

app.get('/minor', middlware, (req, res) => {
  res.render('minor', { nome: req.query.nome });
});

app.get('/major', middlware, (req, res) => {
  res.render('major', { nome: req.query.nome });
});
app.listen(3000);
